<?php


function __autoload($classNameWithNamespace){

    $className = explode("\\",$classNameWithNamespace);
    $classPath="../../src/BITM/SEIP_157006/$className[1].php";

    echo $classPath."<br>";
    require_once($classPath);
}

if (empty($_POST['studentID'])){
    echo "I'm a Person <br>";

    $obj = new \App\Person();

    $obj -> setName($_POST['userName']);
    $obj -> setDateOfBirth($_POST['dateOfBirth']);

    echo $obj -> getName() . "<br>";
    echo $obj -> getDateOfBirth() . "<br>";
}
else{
    echo "I'm a Student <br>";

    $obj = new \Tap\Student();

    $obj -> setName($_POST['userName']);
    $obj -> setDateOfBirth($_POST['dateOfBirth']);
    $obj -> setStudentID($_POST['studentID']);

    echo $obj -> getName() . "<br>";
    echo $obj -> getDateOfBirth() . "<br>";
    echo $obj -> getStudentID() . "<br>";


}